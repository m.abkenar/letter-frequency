#!/bin/bash

## checking input file:
if [ $# -eq 0 ]; then echo ""; echo "No input text file is given. Aborting."; echo "Usage:"; echo "    bash letter-frequency.sh input_file.txt"; echo ""; exit 1; fi
inputfile=$1
if [ ! -e "${inputfile}" ]; then echo ""; echo "File '"${inputfile}"' not found. Please replace it with the text file you want to analyze. Aborting."; echo ""; exit 1;fi

## naming output files:
datafile=${inputfile%.*}-letter-frequency.dat
outputfile=${inputfile%.*}-letter-frequency.pdf
tempfile="temp.dat"
if [ -e "$tempfile" ]; then rm "$tempfile"; fi
touch "$tempfile"

## Letters to be analyzed:
# for English:
allLetters="1 2 3 4 5 6 7 8 9 0 . : , ; a b c d e f g h  j k l m n o p q r s t u v w x y z " 
## for Persian:
#allLetters="۱ ۲ ۳ ۴ ۵ ۶ ۷ ۸ ۹ ۰ ‌ . : ، ؛ آ ا ب  پ ت ث ج چ ح خ د ذ ر ز ژ س ش ص ض ط ظ ع غ ک گ ل م ن و ه ی ئ ؤ ء";



## loop over all letters:
for l in $allLetters
do
	freq=`fgrep -c -o $l "${inputfile}"`
	if [ $l = "‌" ]; then l="⟙"; fi;	# replace ZWNJ with a symbol in the output data file
	echo $l"	"$freq >> "$tempfile"
done

## sorting letters based on frequency:
sort -rn -k2 "$tempfile" > "$datafile"
rm "${tempfile}"
echo ""
echo "Letter frequency data is written in '"$datafile"'."

## plotting the data:
gnuplot  <<- EOP
inputfile="${inputfile}"
datafile="${datafile}"
outputfile="${outputfile}"

set t pdf enhanced size 7,2 font "pfont"
set output outputfile

set lmargin at screen 0.1
set boxwidth 0.9 relative
set style data histograms
set style histogram cluster
set style fill solid 1.0 border lt -1

set xtics scale 0
set ylabel "letter frequency"

p datafile u 2:xticlabels(1) t inputfile

set output
pr "Plot is saved in '".outputfile."'."
EOP

echo ""
